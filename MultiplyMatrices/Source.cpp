#include <cstdlib>
#include <math.h>
#include <iostream>
#include <ctime>
#include <vector>
#include <thread>

#define ROWS 8192
#define COLS 8192
using namespace std;

void multiply(int** matrix, int* vector, int* product)
{

	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			product[i] += (matrix[i][j] * vector[j]);
		}
	}
}

void multiply_thread(int** matrix, int* vector, int* product, int start, int cuts)
{
	for (int i = start; i < start + ROWS / cuts; i++) {
		for (int j = 0; j < COLS; j++) {
			product[i] += (matrix[i][j] * vector[j]);
		}
	}
}

int main()
{
	int i, j;
	int** random_matrix = new int* [ROWS];
	for (i = 0; i < ROWS; i++)
		random_matrix[i] = new int[COLS];
	int* random_vector= new int [ROWS];
	int* product = new int [ROWS];


	srand(static_cast<unsigned>(time(0)));


	for(i = 0; i < ROWS ; i++)
		for (j = 0; j < COLS; j++)
		{
			random_matrix[i][j] = rand() % 1000;
		}

	for (i = 0; i < ROWS; i++)
		random_vector[i] = rand() % 1000;

	clock_t begin = clock();
	cout << "Multiplying 8192x8192 random int matrix by 8192 int vector, 1 thread" << endl;

	multiply(random_matrix, random_vector, product);

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << "Elapsed secs: " << elapsed_secs << endl;

	clock_t begin2 = clock();
	cout << "Multiplying 8192x8192 random int matrix by 8192 int vector, 2 threads" << endl;

	thread t1(multiply_thread, random_matrix, random_vector, product, 0, 2);
	thread t2(multiply_thread, random_matrix, random_vector, product, 4096, 2);
	t1.join();
	t2.join();

	clock_t end2 = clock();
	double elapsed_secs2 = double(end2 - begin2) / CLOCKS_PER_SEC;
	cout << "Elapsed secs: " << elapsed_secs2 << endl;

	clock_t begin4 = clock();
	cout << "Multiplying 8192x8192 random int matrix by 8192 int vector, 4 threads" << endl;

	thread t3(multiply_thread, random_matrix, random_vector, product, 0, 4);
	thread t4(multiply_thread, random_matrix, random_vector, product, 2048, 4);
	thread t5(multiply_thread, random_matrix, random_vector, product, 4096, 4);
	thread t6(multiply_thread, random_matrix, random_vector, product, 6144, 4);
	t3.join();
	t4.join();
	t5.join();
	t6.join();

	clock_t end4 = clock();
	double elapsed_secs4 = double(end4 - begin4) / CLOCKS_PER_SEC;
	cout << "Elapsed secs: " << elapsed_secs4 << endl;


	for (i = 0; i < ROWS; i++)
		delete random_matrix[i];
	delete random_matrix;
	delete random_vector;
	delete product;

	return 0;
}